<?php

$i = 1;
    
	if ( mt_rand(1,4) === mt_rand(1,4) ) {
		if ( $i < 4 ) {

		
			$promo_args = array(
				'post_type' => 'promos',
				'orderby' => 'rand',
				'posts_per_page' => 1,
			);
			
			$promos_loop = new WP_Query( $promo_args );
			
			if ( $promos_loop->have_posts()){
				while ( $promos_loop->have_posts()){
					$promos_loop->the_post();
			?>
			
			
					<div class="yd-promo-container <?php the_field('promo_color'); ?>">
						<a href="<?php the_field('button_url'); ?>"  class="promo-link" <?php if(get_field('promo_new_window') == 1 ){?> target="_blank" <?php } ?>>
							<div class="promo-vertical">
								<div class="panel panel-<?php the_field('promo_color'); ?>">
									<div class="panel-heading">
										<h3 class="panel-title"><?php the_field('promo_headline'); ?></h3>
										<?php if( !empty( get_field('promo_date') ) ) : ?><p class="text-center"><strong><?php
										$date = get_field('promo_date', false, false);
										$date = new DateTime($date);
										echo $date->format('F j, Y');
											?></strong></p><?php endif; ?>
									</div>
									<div class="panel-body">
										<?php
											$attachment_id = get_field('promo_image');
											$size = "promo-thumb";
											$image = wp_get_attachment_image_src( $attachment_id, $size );
										?>

										<img class="img-responsive" src="<?php echo $image[0]; ?>">
									</div>
									<div class="panel-footer">
										<p><?php the_field('promo_body'); ?></p>
												<span class="button btn-clear btn-block"><?php the_field('button_text'); ?> <i class="fa fa-arrow-circle-right"></i></span>
									</div>
								</div>
							</div>
						</a>
					</div>

				<?php
					$i++;
				}
		wp_reset_postdata();
			}
		}
	}

	?>