<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php if ( is_sticky() ) : ?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'yd-sticky-news-article' ); ?>>

    <div class="col-xs-12 col-sm-8">

        <?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
        <div class="yd-post-thumbnail">
            <?php if ( has_post_thumbnail()) : ?>
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <?php the_post_thumbnail('thumbnail', ['class' => 'img-responsive']); ?>
            </a>
            <?php endif; ?>
        </div><!-- .yd-post-thumbnail -->
        <?php elseif( '' === get_the_post_thumbnail() && ! is_single()) : ?>
        <div class="yd-post-thumbnail">
            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                <img class="img-responsive" src="/wp-content/uploads/default-thumbnail.jpg" alt="">
            </a>
        </div>
        <?php endif; ?>

    </div>

    <div class="col-xs-12 col-sm-4">

        <header class="yd-entry-header">
            <?php the_title( '<h2 class="yd-entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
        </header><!-- .yd-entry-header -->

        <div class="yd-entry-content">
            <?php
                /* translators: %s: Name of current post */
                the_excerpt();
                // echo '<p>' . get_the_term_list( $post->ID, 'news-type', '', ', ', '' ) . '</p>';
                echo '<p>' . get_modified_term_list( $post->ID, 'news-type', 'Category: ', ', ', '', array('sticky') ) . '</p>';

                wp_link_pages( array(
                    'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
                    'after'       => '</div>',
                    'link_before' => '<span class="page-number">',
                    'link_after'  => '</span>',
                ) );
            ?>
        </div><!-- .yd-entry-content -->
    
    </div>

<?php else : ?>

<article id="post-<?php the_ID(); ?>" <?php post_class( 'yd-news-card' ); ?>>

    <?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
    <div class="yd-post-thumbnail">
        <?php if ( has_post_thumbnail()) : ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <?php the_post_thumbnail('thumbnail', ['class' => 'img-responsive']); ?>
        </a>
        <?php endif; ?>
    </div>
    <?php elseif( '' === get_the_post_thumbnail() && ! is_single()) : ?>
    <div class="yd-post-thumbnail">
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <img class="img-responsive" src="/wp-content/uploads/default-thumbnail.jpg" alt="">
        </a>
    </div>
    <?php endif; ?>

	<header class="yd-entry-header">
		<?php the_title( '<h2 class="yd-entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
	</header><!-- .yd-entry-header -->

	<div class="yd-category-container">
		<?php
        // echo '<p>' . get_the_term_list( $post->ID, 'news-type', 'Category: ', ', ', '' ) . '</p>';
        echo '<p>' . get_modified_term_list( $post->ID, 'news-type', 'Category: ', ', ', '', array('sticky') ) . '</p>';


			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .yd-category-container -->
<?php endif; ?>

	<?php if ( is_single() ) : ?>
		<?php twentyseventeen_entry_footer(); ?>
	<?php endif; ?>

</article><!-- #post-## -->


<?php

    global $promo_count;
    // $i = 1;

    if ( mt_rand(1,3) === 1 ) {
        if ( $promo_count < 4 ) {

            $promo_count++;

            $promo_args = array(
                'post_type' => 'promos',
                'orderby' => 'rand',
                'posts_per_page' => 1,
            );
            
            $promos_loop = new WP_Query( $promo_args );
            
            if ( $promos_loop->have_posts()){
                while ( $promos_loop->have_posts()){
                    $promos_loop->the_post();
            ?>
            
            
                    <div class="yd-promo-container <?php the_field('promo_color'); ?>">
                        <a href="<?php the_field('button_url'); ?>"  class="promo-link" <?php if(get_field('promo_new_window') == 1 ){?> target="_blank" <?php } ?>>
                            <div class="promo-vertical">
                                <div class="panel panel-<?php the_field('promo_color'); ?>">
                                    <div class="panel-heading">
                                        <h3 class="panel-title"><?php the_field('promo_headline'); ?></h3>
                                        <?php if( !empty( get_field('promo_date') ) ) : ?><p class="text-center"><strong><?php
                                        $date = get_field('promo_date', false, false);
                                        $date = new DateTime($date);
                                        echo $date->format('F j, Y');
                                            ?></strong></p><?php endif; ?>
                                    </div>
                                    <div class="panel-body">
                                        <?php
                                            $attachment_id = get_field('promo_image');
                                            $size = "promo-thumb";
                                            $image = wp_get_attachment_image_src( $attachment_id, $size );
                                        ?>

                                        <img class="img-responsive" src="<?php echo $image[0]; ?>">
                                    </div>
                                    <div class="panel-footer">
                                        <p><?php the_field('promo_body'); ?></p>
                                                <span class="button btn-clear btn-block"><?php the_field('button_text'); ?> <i class="fa fa-arrow-circle-right"></i></span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                <?php
                    // $i++;
                }
            wp_reset_postdata();
            }
        }
    }

?>
