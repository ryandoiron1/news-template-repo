<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<?php

if(is_post_type_archive('news') || is_tax( 'news-type' )){

	get_template_part( 'template-parts/post/content-news-archive', get_post_format() );

}

elseif(is_post_type_archive('event')) {
	get_template_part( 'template-parts/post/content-event-archive', get_post_format() );

} elseif(is_post_type_archive('adoption_stories')) {
	get_template_part( 'template-parts/post/content-adoptions-archive', get_post_format() );

}
else {

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php
		if ( is_sticky() && is_home() ) :
			echo twentyseventeen_get_svg( array( 'icon' => 'thumb-tack' ) );
		endif;
	?>
	<header class="entry-header">
		
		
		<?php
			if ( 'post' === get_post_type() ) :
				echo '<div class="entry-meta">';
					if ( is_single() ) :
						twentyseventeen_posted_on();
					else :
						echo twentyseventeen_time_link();
					endif;
				echo '</div><!-- .entry-meta -->';
			endif;

			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}

		?>

	</header><!-- .entry-header -->

	<?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
		<div class="post-thumbnail">
			<a href="<?php the_permalink(); ?>">
				<?php the_post_thumbnail('thumbnail'); ?>
			</a>
		</div><!-- .post-thumbnail -->
	<?php endif; ?>

	<div class="entry-content">

		<?php
		// If a regular post or page, and not the front page, show the featured image.
		if ( has_post_thumbnail() && ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage()) ) ) :
			echo '<div class="single-featured-image-header">';
			the_post_thumbnail( 'twentyseventeen-featured-image' );
			echo '</div><!-- .single-featured-image-header -->';
			the_excerpt();

		endif;

			/* translators: %s: Name of current post */
			// if ( is_post_type_archive('news') ) {
			// 	echo '<p>' . get_the_term_list( $post->ID, 'news-type', 'News Category: ', ', ', '' ) . '</p>';
			// }


			// the_content( sprintf(
			// 	__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ),
			// 	get_the_title()
			// ) );

			wp_link_pages( array(
				'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
				'after'       => '</div>',
				'link_before' => '<span class="page-number">',
				'link_after'  => '</span>',
			) );
		?>
	</div><!-- .entry-content -->

	<?php if ( is_single() ) : ?>
		<?php twentyseventeen_entry_footer(); ?>
	<?php endif; ?>

</article><!-- #post-## -->
<?php
}
?>
