<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>


<article id="post-<?php the_ID(); ?>" <?php post_class( 'yd-sticky-news-article row' ); ?>>
    <div class="col-xs-12 col-sm-8">


        <?php if ( '' !== get_the_post_thumbnail() && ! is_single() ) : ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <div class="yd-post-thumbnail" style="background-image: url('<?php echo get_the_post_thumbnail_url( $post->ID, 'full' ); ?>');">
            </div>
        </a>



        <?php elseif( '' === get_the_post_thumbnail() && ! is_single()) : ?>
        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
            <div class="yd-post-thumbnail" style="background-image: url('/wp-content/uploads/default-thumbnail.jpg');">
                    <!-- <img class="img-responsive" src="/wp-content/uploads/default-thumbnail.jpg" alt=""> -->
            </div>
        </a>
        <?php endif; ?>



    </div>

    <div class="col-xs-12 col-sm-4">
        <header class="yd-entry-header">
            <?php the_title( '<h2 class="yd-entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
        </header><!-- .yd-entry-header -->
        <div class="yd-category-content">
            <?php the_excerpt(); ?>
        </div>
        <div class="yd-category-container">
            <?php
            // echo '<p>' . get_the_term_list( $post->ID, 'news-type', 'Category: ', ', ', '' ) . '</p>';
            echo '<p>' . get_modified_term_list( $post->ID, 'news-type', 'Category: ', ', ', '', array('sticky') ) . '</p>';


                wp_link_pages( array(
                    'before'      => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
                    'after'       => '</div>',
                    'link_before' => '<span class="page-number">',
                    'link_after'  => '</span>',
                ) );
            ?>
        </div><!-- .yd-category-container -->
    </div>


	<?php if ( is_single() ) : ?>
		<?php twentyseventeen_entry_footer(); ?>
	<?php endif; ?>

</article><!-- #post-## -->