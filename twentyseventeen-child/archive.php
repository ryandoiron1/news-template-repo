<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
	<div id="primary" class="content-area">
        <?php if ( get_post_type( get_the_ID() ) == 'news' ) : ?>
        <main id="main" class="main-content interior-page container archives-page yd-news-archives-page" role="main">
            <div class="col-md-12 content-column">
                <div class="yd-news-topnav-container row">
                    <div class="col-xs-12 col-sm-4 col-sm-push-4">
                        <h1>Categories</h1>
                        <select class="yd-news-categories" onchange="quicklink(this.value)">

                        <?php
                            $args = array( 'hide_empty=1' );

                            $terms = get_terms( 'news-type', $args );
                            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
                                $count = count( $terms );
                                $i = 0;
                                $term_list = '<option value="' . bloginfo('url') . '/news">All</option>';
                                foreach ( $terms as $term ) {
                                    $i++;
                                    $term_list .= '<option value="' . esc_url( get_term_link( $term ) ) . '">' . $term->name . '</option>';
                                    if ( $count != $i ) {
                                        // $term_list .= ' <br>';
                                    }
                                    else {
                                        $term_list .= '</option>';
                                    }
                                }
                                echo $term_list;
                            }
                        ?>

                        </select>

                        <script type="text/javascript">
                            function quicklink(lnk){
                                if(lnk === 'blank') {
                                } else {
                                    window.location.href = lnk;
                                }
                            }

                            jQuery(function($){
                                // change the currently selected category to reflect what is being viewed
                                currentUrl = window.location.href;
                                $('select.yd-news-categories option').each(function(){
                                    $(this).attr('value');
                                    if ( $(this).attr('value') === currentUrl ) {
                                        $(this).prop('selected', true);
                                    }
                                });
                            });
                        </script>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-sm-pull-4">
                    <?php if ( have_posts() ) : ?>
                        <?php echo '<h1>' . str_replace('Archives: ','',get_the_archive_title()) . '</h1>'; ?>
                    <?php endif; ?>
                        <form class="form-inline" method="get" action="<?php bloginfo('url'); ?>">
                            <div class="form-group">
                                <input type="hidden" name="post_type" value="news">
                                <input type="text" name="s" value="" placeholder="Search &hellip;" maxlength="50" class="form-control" required="required" />
                            </div>
                            <button type="submit" class="button btn-green">Search</button>
                        </form>
                    </div>
                    <div class="col-xs-12 col-sm-4 yd-desktop-only">
                        <h1>Follow us</h1>
                        <ul class="yd-nav-social">
                            <li><a href="https://www.facebook.com/bcspca/" target="_blank"><i class="fa fa-2x fa-facebook-square"></i></a></li>
                            <li><a href="http://twitter.com/BC_SPCA" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a></li>
                            <li><a href="http://www.instagram.com/bcspca" target="_blank"><i class="fa fa-2x fa-instagram"></i></a></li>
                            <li><a href="http://www.youtube.com/user/bcspcabc?sub_confirmation=1" target="_blank"><i class="fa fa-2x fa-youtube-square"></i></a></li>
                        </ul>
                    </div>
                </div>

                <!-- START STICKY POST -->
                <?php
                        
                        $sticky_args = array (
                            'post_type' => 'news',
                            'order' => 'ASC',
                            'posts_per_page' => 1,
                            'tax_query' => array(
                                array(
                                        'taxonomy'  => 'news-type',
                                        'field'     => 'slug',
                                        'terms'     => 'sticky',
                                        'operator'  => 'IN'
                                    )
                            ),
                        );

                        $sticky_query = new WP_Query( $sticky_args );

                ?>
                <?php if ( $sticky_query->have_posts() ) : ?>
                    <?php

                    global $promo_count;
                    $promo_count = 1;
                    /* Start the Loop */
                    while ( $sticky_query->have_posts() ) : $sticky_query->the_post();

                        get_template_part( 'template-parts/post/sticky-post' );


                    endwhile; ?>
                <?php wp_reset_postdata(); ?>
                <?php endif; ?>
                <!-- END STICKY POST -->

                <?php if ( have_posts() ) : ?>
                    <?php
                    /* Start the Loop */
                    while ( have_posts() ) : the_post();

                        get_template_part( 'template-parts/post/archive', get_post_format() );

                    endwhile; ?>

                    <!-- START LOAD MORE BUTTON -->
                    <?php
                    
                    // don't display the button if there are not enough posts
                    if (  $wp_query->max_num_pages > 1 )
                        echo '<div class="yd_loadmore_container"><button class="yd_loadmore" id="yd_loadmore_all_news">See more stories</button></div>';
                    ?>
                    <!-- END LOAD MORE BUTTON -->
                <?php else :

                    get_template_part( 'template-parts/post/content', 'none' );

                endif; ?>

                <!-- START News signup and follow us blocks -->
                <div class="yd-signup-block row">
                    <div class="col-xs-12 col-sm-8">
                        <h2>Subscribe to get BC SPCA news and updates</h2>
                        <script src="//cdnjs.cloudflare.com/ajax/libs/luminateExtend/1.8.1/luminateExtend.min.js"></script>
                        <script type="text/javascript">
                        (function($) {
                            /* define init variables for your organization */
                            luminateExtend({
                                apiKey: 'open_bcspca',
                                path: {
                                    nonsecure: 'http://support.spca.bc.ca/site/',
                                    secure: 'https://secure3.convio.net/bcspca/site/'
                                }
                            });


                            $(function() {

                                /* UI handlers for the Survey example */
                                if($('.news-form').length > 0) {
                                    $('.news-form').submit(function() {
                                        //window.scrollTo(0, 0);
                                        $(this).hide();
                                        $(this).before('<div class="well news-loading" style="color:white!important;">' +
                                            'Loading ...' +
                                            '</div>');
                                    });
                                }

                                /* example: handle the Survey form submission */
                                /* if the Survey is submitted succesfully, display a thank you message */
                                /* if there is an error, display it inline */
                                window.submitNewsCallback = {
                                    error: function(data) {
                                        $('#news-errors').remove();
                                        $('.news-form .form-group .news-alert-wrap').remove();

                                        $('.news-loading').remove();

                                        $('.news-form').show();

                                        if (data.errorResponse.code){

                                            switch(data.errorResponse.code){
                                                case "1725":
                                                    errorMessage = "Error code " + data.errorResponse.code + ": You've already signed up to receive updates to this email address.";
                                                    break;
                                                default:
                                                    errorMessage = "Error code " + data.errorResponse.code + ": " + data.errorResponse.message;
                                            }

                                            jQuery('.news-form').prepend('<p class="alert-danger alert" id="news-errors">' + errorMessage + '</p>');

                                        }

                                        if(data.errorResponse.fieldError) {
                                            var fieldErrors = luminateExtend.utils.ensureArray(data.errorResponse.fieldError);
                                            $.each(fieldErrors, function() {
                                                $('#donation-errors').append('<div class="alert alert-danger">' +
                                                    this +
                                                    '</div>');
                                            });
                                        }

                                    },
                                    success: function(data) {

                                        
                                        $('#news-errors').remove();
                                        $('.news-form .form-group .news-alert-wrap').remove();

                                        if(data.submitSurveyResponse.success == 'false') {
                                            $('.news-form').prepend('<div id="news-errors">' +
                                                '<div class="alert alert-danger">' +
                                                'There was an error with your submission. Please try again.' +
                                                '</div>' +
                                                '</div>');

                                            var surveyErrors = luminateExtend.utils.ensureArray(data.submitSurveyResponse.errors);
                                            $.each(surveyErrors, function() {
                                                if(this.errorField) {
                                                    $('input[name="' + this.errorField + '"]').closest('.form-group')
                                                        .prepend('<div class="col-sm-12 news-alert-wrap">' +
                                                            '<div class="alert alert-danger">' +
                                                            this.errorMessage +
                                                            '</div>' +
                                                            '</div>');
                                                }
                                            });

                                            $('.news-loading').remove();
                                            $('.news-form').show();
                                        }
                                        else {
                                            dataLayer.push({
                                                'event':'wpNewsletterSignup',
                                                'eventCategory':'Newsletter Signup',
                                                'eventAction':'get_updates_newsletter',
                                                'eventLabel': window.location.href // Page URL where form is submitted
                                            });
                                            $('.news-loading').remove();
                                            $('.news-form').before('<div class="alert alert-success">' +
                                                'You\'ve been signed up!' +
                                                '</div>' +
                                                '<div class="well">' +
                                                '<p>Thanks for joining- you should receive your first newsletter shortly.</p>' +
                                                '</div>');
                                        }
                                    }
                                };



                                /* bind any forms with the "luminateApi" class */
                                luminateExtend.api.bind("#news-form");
                            });
                        })(jQuery);
                        </script>


                        <div class="news_form_wrapper">
                            <form id="news-form" class="form-style luminateApiAlt news-form" action="https://secure3.convio.net/bcspca/site/CRSurveyAPI" method="POST" data-luminateapi="{'callback': 'submitNewsCallback', 'requiresAuth': 'true'}">
                                <input name="method" type="hidden" value="submitSurvey">
                                <input name="survey_id" type="hidden" value="1221">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row align-items-center">
                                            <div class="input-margins-forms col-sm-12 col-md-4">
                                                <div class="form-label-group-float input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="fa fa-2x fa-envelope"></i></span>
                                                    </div>
                                                    <label class="control-label" for="cons_email_news">Email Address</label>
                                                    <input id="cons_email_news" class="form-control" name="cons_email" required="required" type="email" placeholder="Email Address">
                                                </div>
                                            </div>
                                            <div class="input-margins-forms col-sm-6 col-md-4">
                                                <div class="form-label-group-float">
                                                    <label class="control-label" for="cons_first_name_news">First Name</label>
                                                    <input id="cons_first_name_news" class="form-control" name="cons_first_name" type="text" placeholder="First Name">
                                                </div>
                                            </div>
                                            <div class="input-margins-forms col-sm-6 col-md-4">
                                                <div class="form-label-group-float">
                                                    <label class="control-label" for="cons_last_name_news">Last Name</label>
                                                    <input id="cons_last_name_news" class="form-control" name="cons_last_name" type="text" placeholder="Last Name">
                                                </div>
                                            </div>
                                        </div>
                                        <input id="question_12343_1" name="question_12343" type="hidden" value="2001">
                                        <input id="question_12343_2" name="question_12343" type="hidden" value="1021">
                                        <input id="question_12343_3" name="question_12343" type="hidden" value="2241">
                                        <input id="question_12343_4" name="question_12343" type="hidden" value="2562">
                                        <input id="question_12343_5" name="question_12343" type="hidden" value="2421">
                                        <input id="question_12343_6" name="question_12343" type="hidden" value="1121">
                                        <input id="question_12343_7" name="question_12343" type="hidden" value="2462">
                                        <input id="question_12343_8" name="question_12343" type="hidden" value="1081">
                                        <input id="question_12343_9" name="question_12343" type="hidden" value="1244">
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6 text-center">
                                                <button id="survey-submit-news" class="gform_button button btn-block" type="submit" onclick="fbq('track', 'Subscribe');">Subscribe</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <h2>Follow us</h2>
                        <ul class="yd-nav-social">
                            <li><a href="https://www.facebook.com/bcspca/" target="_blank"><i class="fa fa-2x fa-facebook-square"></i></a></li>
                            <li><a href="http://twitter.com/BC_SPCA" target="_blank"><i class="fa fa-2x fa-twitter-square"></i></a></li>
                            <li><a href="http://www.instagram.com/bcspca" target="_blank"><i class="fa fa-2x fa-instagram"></i></a></li>
                            <li><a href="http://www.youtube.com/user/bcspcabc?sub_confirmation=1" target="_blank"><i class="fa fa-2x fa-youtube-square"></i></a></li>
                        </ul>
                    </div>
                </div>
                <!-- END News signup and follow us blocks -->

                <!-- START togglable news category blocks -->
                <?php 

                // get ACF from News Stories Settings option page
                $categories = get_field('additional_news_categories', 'option');

                // check if any categories are checked..
                if( $categories ): ?>
                    <?php foreach( $categories as $category ): ?>
                        <?php
                        
                            $paged = ( get_query_var('page') ) ? get_query_var('page') : 1;
                            $category_args = array (
                                'post_type' => 'news',
                                'order' => 'ASC',
                                'posts_per_page' => 4,
                                'tax_query' => array(
                                    array(
                                            'taxonomy'  => 'news-type',
                                            'field'     => 'slug',
                                            'terms'     => $category['value'],
                                            'operator'  => 'IN'
                                        )
                                ),
                            );

                            $category_query = new WP_Query( $category_args );

                        ?>
                        <?php if ( $category_query->have_posts() ) : ?>
                            <h1 class="clearfix"><?php echo $category['label']; ?></h1>
                            <?php
                            /* Start the Loop */
                            while ( $category_query->have_posts() ) : $category_query->the_post();

                                get_template_part( 'template-parts/post/archive', get_post_format() );


                            endwhile; ?>

                            <!-- START LOAD MORE BUTTON -->
                            <?php
                            
                            // don't display the button if there are not enough posts
                            if (  $category_query->max_num_pages > 1 )
                                echo '<div class="yd_loadmore_container"><button class="yd_loadmore" id="yd_loadmore_news_' . str_replace("-","_",$category['value']) . '">See more stories</button></div>';
                            ?>
                            <!-- END LOAD MORE BUTTON -->

                            <script>
                                var posts_<?php echo str_replace("-","_",$category['value']) ?> = '<?php echo json_encode( $category_query->query_vars ); ?>',
                                current_page_<?php echo str_replace("-","_",$category['value']) ?> = <?php echo $category_query->query_vars['paged']; ?>,
                                max_page_<?php echo str_replace("-","_",$category['value']) ?> = <?php echo $category_query->max_num_pages; ?>;

                                jQuery(function($){

                                    function getMorePosts( post_category, current_page, max_page, element ) {
                                        var button = $(element),
                                            data = {
                                            'action': 'loadmore',
                                            'query': post_category,
                                            'page' : current_page
                                        };

                                        $.ajax({
                                            url : '/wp-admin/admin-ajax.php',
                                            data : data,
                                            type : 'POST',
                                            beforeSend : function ( xhr ) {
                                                button.text('Loading...');
                                            },
                                            success : function( data ){
                                                if( data ) { 
                                                    button.text( 'See more stories' ).parent().prev().after(data);
                                                    current_page++;

                                                    if ( current_page == max_page ) {
                                                        button.remove();
                                                    }

                                                } else {
                                                    button.remove();
                                                }
                                            }
                                        });
                                    }

                                    $('#yd_loadmore_news_adoption').click(function(){
                                        getMorePosts(posts_adoption, current_page_adoption, max_page_adoption, this);
                                    });

                                    $('#yd_loadmore_news_animal_cruelty').click(function(){
                                        getMorePosts(posts_animal_cruelty, current_page_animal_cruelty, max_page_animal_cruelty, this);
                                    });

                                    $('#yd_loadmore_news_awards_and_accreditation').click(function(){
                                        getMorePosts(posts_awards_and_accreditation, current_page_awards_and_accreditation, max_page_awards_and_accreditation, this);
                                    });

                                    $('#yd_loadmore_news_community').click(function(){
                                        getMorePosts(posts_community, current_page_community, max_page_community, this);
                                    });

                                    $('#yd_loadmore_news_events').click(function(){
                                        getMorePosts(posts_events, current_page_events, max_page_events, this);
                                    });

                                    $('#yd_loadmore_news_farm').click(function(){
                                        getMorePosts(posts_farm, current_page_farm, max_page_farm, this);
                                    });

                                    $('#yd_loadmore_news_generosity_in_action').click(function(){
                                        getMorePosts(posts_generosity_in_action, current_page_generosity_in_action, max_page_generosity_in_action, this);
                                    });

                                    $('#yd_loadmore_news_pet_care_and_behaviour').click(function(){
                                        getMorePosts(posts_pet_care_and_behaviour, current_page_pet_care_and_behaviour, max_page_pet_care_and_behaviour, this);
                                    });

                                    $('#yd_loadmore_news_take_action').click(function(){
                                        getMorePosts(posts_take_action, current_page_take_action, max_page_take_action, this);
                                    });

                                    $('#yd_loadmore_news_tip_tuesday').click(function(){
                                        getMorePosts(posts_tip_tuesday, current_page_tip_tuesday, max_page_tip_tuesday, this);
                                    });

                                    $('#yd_loadmore_news_ways_to_help').click(function(){
                                        getMorePosts(posts_ways_to_help, current_page_ways_to_help, max_page_ways_to_help, this);
                                    });

                                    $('#yd_loadmore_news_wildlife').click(function(){
                                        getMorePosts(posts_wildlife, current_page_wildlife, max_page_wildlife, this);
                                    });

                                    $('#yd_loadmore_news_youth').click(function(){
                                        getMorePosts(posts_youth, current_page_youth, max_page_youth, this);
                                    });

                                });
                            </script>
                        <?php wp_reset_postdata(); ?>
                        <?php else :

                            get_template_part( 'template-parts/post/content', 'none' );

                        endif; ?>

                    <?php endforeach; ?>
                <?php endif; ?>
                <!-- END togglable news category blocks -->
            </div>
        </main><!-- #main -->
	</div><!-- #primary -->

        <?php else : ?>

		<main id="main" class="main-content interior-page container archives-page" role="main">
			<div class="col-md-9 col-md-push-3 right-column content-column">

			<?php if ( have_posts() ) : ?>
                <?php echo '<h1>' . str_replace('Archives: ','',get_the_archive_title()) . '</h1>'; ?>
			<?php endif; ?>

            <?php
            if ( get_post_type( get_the_ID() ) == 'ufaq' ) {
                echo do_shortcode( '[ultimate-faq-search]' );
            } else {
            ?>
			<form class="form-inline" method="get" action="<?php bloginfo('url'); ?>">
                <div class="form-group">
                <?php if (get_post_type( get_the_ID() ) == 'adoption_stories' ) : ?>
                    <input type="hidden" name="post_type" value="adoption_stories">
                <?php endif; ?>
                    <input type="text" name="s" value="" placeholder="Search &hellip;" maxlength="50" class="form-control" required="required" />
                </div>

			<?php
		    }
			?>
                <button type="submit" class="button btn-blue">Search</button>
			</form>

            <?php
            if ( have_posts() ) : ?>
                <?php
                /* Start the Loop */
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/post/archive', get_post_format() );

                endwhile;

                the_posts_pagination( array(
                    'prev_text' => twentyseventeen_get_svg( array( 'icon' => 'arrow-left' ) ) . '<span class="screen-reader-text">' . __( 'Previous page', 'twentyseventeen' ) . '</span>',
                    'next_text' => '<span class="screen-reader-text">' . __( 'Next page', 'twentyseventeen' ) . '</span>' . twentyseventeen_get_svg( array( 'icon' => 'arrow-right' ) ),
                    'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentyseventeen' ) . ' </span>',
                ) );

            else :

                get_template_part( 'template-parts/post/content', 'none' );

            endif; ?>
            </div>

            <div class="col-md-3 col-md-pull-9 left-column content-column">
                <?php get_sidebar(); ?>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

    <?php endif; ?>

<?php get_footer();