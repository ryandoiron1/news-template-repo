<?php
add_action( 'after_setup_theme', 'remove_parent_theme_features', 10 );

function remove_parent_theme_features() {
    // Unhook default Thematic functions
  function unhook_thematic_functions() {
      remove_action( 'template_redirect', 'twentyseventeen_content_width', 0 );
      $content_width = 825;

      $GLOBALS['content_width'] = apply_filters( 'twentyseventeen_content_width', $content_width );
  }
  add_action( 'init', 'unhook_thematic_functions' );
}

function remove_default_widget(){
  // Remove default Footer 2 widget
	unregister_sidebar( 'sidebar-3' );
  unregister_sidebar('sidebar-2');
}
add_action( 'widgets_init', 'remove_default_widget', 11 );

// add tag support to pages
function tags_support_all() {
	register_taxonomy_for_object_type('post_tag', 'page');
}

// tag hooks
add_action('init', 'tags_support_all');

function child_theme_setup() {
  if ( function_exists( 'add_image_size' ) ) {
    remove_image_size('medium');
    remove_image_size('medium_large');
    remove_image_size('large');
    remove_image_size('twentyseventeen-thumbnail-avatar');
    remove_image_size('twentyseventeen-featured-image');
    add_image_size( 'promo-thumb', 360, 360, true );
    add_image_size( 'adopt-me-thumb', 260, 350 );
    add_image_size( 'emergency-thumb', 500, 360, true );
    add_image_size( 'branch-thumb', 833, 292 );
  }
}

add_action( 'after_setup_theme', 'child_theme_setup', 11 );

// add oasis workflow
// add_action('owf_update_published_page', 'update_post_meta_info', 10, 2);


function filter_image_sizes( $sizes) {
    unset( $sizes['medium']);
    unset( $sizes['large']);
    unset( $sizes['medium_large']);
    unset( $sizes['twentyseventeen-featured-image'] );
    unset( $sizes['twentyseventeen-thumbnail-avatar'] );
    return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'filter_image_sizes');


// Register custom navigation walker
require_once('wp-bootstrap-navwalker.php');
function wpb_add_google_fonts() {
  wp_enqueue_style( 'twentyseventeen-fonts', 'https://fonts.googleapis.com/css?family=Archivo+Narrow:400,400i,700|Lato:300,400', false );

  }


add_action( 'wpb_add_google_fonts', 'twentyseventeen-fonts' );

// Enqueue child-theme custom stylesheet
function tseventeen_child_styles() {

    wp_enqueue_style(
        'twentyseventeen-style',
        get_stylesheet_directory_uri() . '/assets/css/style.css'
    );

    wp_enqueue_style(
        'child-custom-styles',
        get_stylesheet_directory_uri() . '/custom.css',
        array( 'twentyseventeen-style')
    );
	
	//register font-awesome
	wp_enqueue_style( 'font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css', array() );
}
add_action( 'wp_enqueue_scripts', 'tseventeen_child_styles');

// Two attempts at displaying publish button for Rev Dev admin role on Event post types. Both attempts have so far failed.

// Attempt 1: Display Publish to rev dev admins. This is based on default example by plugin provider. does not account for $post_type == 'event' - https://www.oasisworkflow.com/documentation/hooks-and-filters/skip-workflow-filter
// add_filter( 'owf_skip_workflow', 'custom_skip_workflow', 10, 1 );
//
// function custom_skip_workflow($post_id) {
//     if(get_role('revenue_development_staff')){
//         return true;
//     }
//     return false;
// }

// Attempt 2: The example below tries to take into account both the admin role as well as $post_type. This also fails
// function get_admin_posttype( $post_id ) {
//   // change to any custom post type
//   if( get_post_type($post_id) == 'event' ){
//     return true;
//
//   }
// }
//
// add_filter( 'owf_skip_workflow', 'custom_skip_workflow', 10, 1 );
//
// function custom_skip_workflow($post_id) {
//     if(get_role('revenue_development_staff')){
//       add_action('admin_notices', 'get_admin_posttype');
//     }
//     return false;
// }

// override default RSS feed
remove_all_actions( 'do_feed_rss2' );
function news_feed_rss2() {
    load_template('wp-content/themes/twentyseventeen-child/feeds/feed-news-rss2.php');
}
add_action('do_feed_rss2', 'news_feed_rss2', 10, 1);



//function loadCustomJS() {
//
//  wp_enqueue_script( 'twentyseventeen-vendor', get_theme_file_uri( '/assets/js/vendor/modernizr.js' ), array(), '1.0', true );
//
//  wp_enqueue_script( 'twentyseventeen-plugins', get_theme_file_uri( '/assets/js/plugins.js' ), array(), '1.0', true );
//  wp_enqueue_script( 'twentyseventeen-main', get_theme_file_uri( '/assets/js/main.js' ), array(), '1.0', true );
//  // wp_enqueue_script( 'twentyseventeen-donate', get_theme_file_uri( '/assets/js/donate.js' ), array(), '1.0', true );
//
//  wp_enqueue_script( 'luminateExtend', get_theme_file_uri( '/assets/js/luminateExtend.js' ), array('jquery'), '1.0', true );
//  wp_enqueue_script( 'luminate-surveys', get_theme_file_uri( '/assets/js/surveys.js' ), array('jquery'), '1.0', true );
//
//}

function loadCustomJS() {

    wp_enqueue_script(
        'twentyseventeen-vendor',
        get_stylesheet_directory_uri() . '/assets/js/vendor/modernizr.js',
        array( 'jquery' )
    );

    wp_enqueue_script(
        'twentyseventeen-plugins',
        get_stylesheet_directory_uri() . '/assets/js/plugins.js',
        array( 'jquery' )
    );

    wp_enqueue_script(
        'twentyseventeen-main',
        get_stylesheet_directory_uri() . '/assets/js/main.js',
        array( 'jquery' )
    );

    wp_enqueue_script(
        'luminateExtend',
        get_stylesheet_directory_uri() . '/assets/js/luminateExtend.js',
        array( 'jquery' )
    );

    wp_enqueue_script(
        'luminate-surveys',
        get_stylesheet_directory_uri() . '/assets/js/surveys.js',
        array( 'jquery' )
    );
}

add_action( 'wp_enqueue_scripts', 'loadCustomJS' );

function add_allowed_origins( $origins ) {
    $origins[] = 'https://secure3.convio.net';
    return $origins;
}

add_filter( 'allowed_http_origins', 'add_allowed_origins' );

// Remove the default 'register_nav_menus' function
function remove_register_nav_menus() {
    remove_action( 'twentyseventeen_setup', 'register_nav_menus' );
}

// Call 'remove_register_nav_menus' (above) during WP initialization
add_action('after_setup_theme','remove_register_nav_menus');

// Register new 'register_nav_menus' function by running 'register_nav_menus' on the 'twentyseventeen_setup' hook. */
add_action( 'twentyseventeen_setup', 'register_nav_menus' );

// This child theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
  'top'    => __( 'Top Menu', 'twentyseventeen' ),
  'social' => __( 'Social Links Menu', 'twentyseventeen' ),
  'footer'    => __( 'Footer Menu', 'twentyseventeen' ),
) );

// override sidebar menus for archive pages

function register_custom_sidebar_nav() {
  add_post_type_support( 'locations', 'simple-page-sidebars' );
  add_post_type_support( 'news', 'simple-page-sidebars' );
  add_post_type_support( 'emergency', 'simple-page-sidebars' );
  add_post_type_support( 'adoption_stories', 'simple-page-sidebars' );
  add_post_type_support( 'ufaq', 'simple-page-sidebars' );
  add_post_type_support( 'archive', 'simple-page-sidebars' );
}
add_action( 'init', 'register_custom_sidebar_nav' );

// order all location results alphabetically
function gjm_change_default_orderby() {
	return 'distance';
}
add_filter( 'gjm_default_orderby', 'gjm_change_default_orderby' );

// wrap youtube videos in responsive div
add_filter('embed_oembed_html', 'wrap_embed_with_div', 10, 3);

function wrap_embed_with_div($html, $url, $attr) {
        return "<div class=\"responsive-container\">".$html."</div>";
}

// order events archive page
// function order_sticky_events( $query ) {
// 	// do not modify queries in the admin
// 	if( is_admin() ) {
// 		return $query;
// 	}
// 	// only modify queries for 'event' post type
// 	if( isset($query->query_vars['post_type']) && $query->query_vars['post_type'] == 'event' ) {
//     $query->set('meta_key', 'post_is_sticky' );
//     $query->set('orderby', array('meta_value' => 'DESC', 'date' => 'DESC'));
// 	}
// 	return $query;
// }
//
// add_action('pre_get_posts', 'order_sticky_events');


// remove "Archives:" from titles of archive pages
add_filter( 'get_the_archive_title', function ($title) {

    if ( is_category() ) {

            $title = single_cat_title( '', false );

        } elseif ( is_tag() ) {

            $title = single_tag_title( '', false );

        } elseif ( is_author() ) {

            $title = '<span class="vcard">' . get_the_author() . '</span>' ;

        }

    return $title;

});


// Exclude news category from news archive page
function exclude_news_cat( $query ) {
	if ( !is_admin() && $query->is_main_query() ) {
		if ( is_post_type_archive( 'news' ) ) {
      $tax_query = $query->get( 'tax_query' ) ?: array();
			$meta_query = $query->get( 'meta_query' ) ?: array();

			$tax_query[] = array(
				'taxonomy' => 'news-type',
				'field'    => 'id',
				'terms'    => array( 93 ),
				'operator' => 'NOT IN',
			);

      $meta_query[] = array(
      	'relation' => 'OR',
      	array(
      		'key'     => 'news_exclude_news_feed',
      		'compare' => 'NOT EXISTS'
      	),
      	array(
          'key' => 'news_exclude_news_feed',
          'value' => 1,
          'compare' => '!=',
          'type' => 'BINARY',
      	)
      );

      $query->set( 'tax_query', $tax_query );
			$query->set( 'meta_query', $meta_query );
		}
	}

	return $query;
}
add_action('pre_get_posts','exclude_news_cat');

// change name of GMW submit button in header
function gmw_modify_submit_text( $labels, $gmw ) {
	$labels['search_form']['submit'] = 'GO &#xf002;';
	return $labels;
}
add_filter( 'gmw_set_labels', 'gmw_modify_submit_text', 50, 2 );


// change kilometers spelling to kilometres
function gmw_modify_kilometers_text( $labels, $gmw ) {
	$labels['search_form']['kilometers'] = 'Kilometres';
	return $labels;
}
add_filter( 'gmw_set_labels', 'gmw_modify_kilometers_text', 50, 2 );


function yoasttobottom() {
	return 'low';
}

add_filter( 'wpseo_metabox_prio', 'yoasttobottom');

add_action('admin_head', 'override_relationship_acf');

function override_relationship_acf() {
  echo '<style>
  /*ADD TO ADMIN STYLESHEET*/
  .post-type-promos .acf_relationship {
    display:block !important;
  }
  </style>';
}


// Register HOME Custom Post Type
function home_post_type() {

	$labels = array(
		'name'                  => _x( 'Home Option', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Home Options', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Home Options', 'text_domain' ),
		'name_admin_bar'        => __( 'Home Options', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Home Options', 'text_domain' ),
		'description'           => __( 'Contains data for home page display', 'text_domain' ),
		'labels'                => $labels,
  	'supports'              => array( 'thumbnail', 'revisions', 'post-formats', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 7,
		'menu_icon'             => 'dashicons-admin-home',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'home', $args );

}
add_action( 'init', 'home_post_type', 0 );

// Register WAYS TO GIVE Custom Post Type
function waystogive_post_type() {

	$labels = array(
		'name'                  => _x( 'Ways to Give', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Ways to Give', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Ways to Give', 'text_domain' ),
		'name_admin_bar'        => __( 'Ways to Give', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Ways to Give Options', 'text_domain' ),
		'description'           => __( 'Contains data for home page display', 'text_domain' ),
		'labels'                => $labels,
    'supports'              => array( 'title', 'page-attributes', 'post-formats', ),
		// 'supports'              => array( 'thumbnail', 'revisions', 'post-formats', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 8,
		'menu_icon'             => 'dashicons-image-filter',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'waystogive', $args );

}
add_action( 'init', 'waystogive_post_type', 0 );



// Register WAYS TO HELP Custom Post Type
function waystohelp_post_type() {

	$labels = array(
		'name'                  => _x( 'Ways to Help', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Ways to Help', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Ways to Help', 'text_domain' ),
		'name_admin_bar'        => __( 'Ways to Help', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Ways to Help Options', 'text_domain' ),
		'description'           => __( 'Contains data for home page display', 'text_domain' ),
		'labels'                => $labels,
    'supports'              => array( 'title', 'page-attributes', 'post-formats', ),
		// 'supports'              => array( 'thumbnail', 'revisions', 'post-formats', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 8,
		'menu_icon'             => 'dashicons-image-filter',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'waystohelp', $args );

}
add_action( 'init', 'waystohelp_post_type', 0 );


// Register WAYS TO GIVE Custom Post Type
function events_fundraisers_post_type() {

	$labels = array(
		'name'                  => _x( 'Events & Fundraisers', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Events & Fundraisers', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Events & Fundraisers', 'text_domain' ),
		'name_admin_bar'        => __( 'Events & Fundraisers', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Events & Fundraisers Options', 'text_domain' ),
		'description'           => __( 'Contains data for home page display', 'text_domain' ),
		'labels'                => $labels,
    'supports'              => array( 'title', 'page-attributes', 'post-formats', ),
		// 'supports'              => array( 'thumbnail', 'revisions', 'post-formats', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => false,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 8,
		'menu_icon'             => 'dashicons-image-filter',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => false,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'events_fundraisers', $args );

}
add_action( 'init', 'events_fundraisers_post_type', 0 );

// Register NEWS STORIES Custom Post Type
function news_post_type() {

	$labels = array(
		'name'                  => _x( 'News Stories', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'News Story', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'News Stories', 'text_domain' ),
		'name_admin_bar'        => __( 'News Story', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All News Stories', 'text_domain' ),
		'add_new_item'          => __( 'Add New Story', 'text_domain' ),
		'add_new'               => __( 'Add New Story', 'text_domain' ),
		'new_item'              => __( 'New Story', 'text_domain' ),
		'edit_item'             => __( 'Edit Story', 'text_domain' ),
		'update_item'           => __( 'Update Story', 'text_domain' ),
		'view_item'             => __( 'View Story', 'text_domain' ),
		'view_items'            => __( 'View Stories', 'text_domain' ),
		'search_items'          => __( 'Search Story', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'News list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'News Story', 'text_domain' ),
		'description'           => __( 'Latest news from BC SPCA', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'tags', 'thumbnail',  'revisions', 'page-attributes', ),
    'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 8,
		'menu_icon'             => 'dashicons-media-text',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'news', $args );

}
add_action( 'init', 'news_post_type', 0 );





// Register PROMO Custom Post Type
function promo_post_type() {

	$labels = array(
		'name'                  => _x( 'Promos', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Promo', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Promos', 'text_domain' ),
		'name_admin_bar'        => __( 'Promos', 'text_domain' ),
		'archives'              => __( 'Promo Archives', 'text_domain' ),
		'attributes'            => __( 'Promo Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Promos', 'text_domain' ),
		'add_new_item'          => __( 'Add New Promo', 'text_domain' ),
		'add_new'               => __( 'Add New Promo', 'text_domain' ),
		'new_item'              => __( 'New Promo', 'text_domain' ),
		'edit_item'             => __( 'Edit Promo', 'text_domain' ),
		'update_item'           => __( 'Update Promo', 'text_domain' ),
		'view_item'             => __( 'View Promo', 'text_domain' ),
		'view_items'            => __( 'View Promos', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Promo', 'text_domain' ),
		'description'           => __( 'Promo blocks for site', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'page-attributes'),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 9,
		'menu_icon'             => 'dashicons-layout',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
		'show_in_rest' => true,
	);
	register_post_type( 'promos', $args );

}
add_action( 'init', 'promo_post_type', 0 );


// Register SPONSOR Custom Post Type
function sponsor_post_type() {

	$labels = array(
		'name'                  => _x( 'Sponsors', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Sponsor', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Sponsors', 'text_domain' ),
		'name_admin_bar'        => __( 'Sponsors', 'text_domain' ),
		'archives'              => __( 'Sponsor Archives', 'text_domain' ),
		'attributes'            => __( 'Sponsor Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Sponsors', 'text_domain' ),
		'add_new_item'          => __( 'Add New Sponsor', 'text_domain' ),
		'add_new'               => __( 'Add New Sponsor', 'text_domain' ),
		'new_item'              => __( 'New Sponsor', 'text_domain' ),
		'edit_item'             => __( 'Edit Sponsor', 'text_domain' ),
		'update_item'           => __( 'Update Sponsor', 'text_domain' ),
		'view_item'             => __( 'View Sponsor', 'text_domain' ),
		'view_items'            => __( 'View Sponsors', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Sponsor', 'text_domain' ),
		'description'           => __( 'Sponsor blocks for site', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'page-attributes'),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 9,
		'menu_icon'             => 'dashicons-welcome-widgets-menus',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => false,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'sponsors', $args );

}
add_action( 'init', 'sponsor_post_type', 0 );

// Register Location Post Type
function location_post_type() {

	$labels = array(
		'name'                  => _x( 'Locations', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Location', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Branch Locations', 'text_domain' ),
		'name_admin_bar'        => __( 'Locations', 'text_domain' ),
		'archives'              => __( 'Location Archives', 'text_domain' ),
		'attributes'            => __( 'Location Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Locations', 'text_domain' ),
		'add_new_item'          => __( 'Add New Location', 'text_domain' ),
		'add_new'               => __( 'Add New Location', 'text_domain' ),
		'new_item'              => __( 'New Location', 'text_domain' ),
		'edit_item'             => __( 'Edit Location', 'text_domain' ),
		'update_item'           => __( 'Update Location', 'text_domain' ),
		'view_item'             => __( 'View Location', 'text_domain' ),
		'view_items'            => __( 'View Locations', 'text_domain' ),
		'search_items'          => __( 'Search Location', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Locations list', 'text_domain' ),
		'items_list_navigation' => __( 'Locations list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter locations list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Location', 'text_domain' ),
		'description'           => __( 'Contains affiliate locations', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions' ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 10,
		'menu_icon'             => 'dashicons-location-alt',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
    // custom capabilities for branch locations - added to grant access to Branch Staff - https://www.role-editor.com/forums/topic/restrict-custom-post-type-testimonials/
    // 'capabilities' =>  array(
    //   'create_posts'=>'create_branch_locations',
    //   'delete_others_posts'=>'delete_others_branch_locations',
    //   'delete_private_posts'=>'delete_private_branch_locations',
    //   'delete_published_posts'=>'delete_published_branch_locations',
    //   'edit_others_posts'=>'edit_others_branch_locations',
    //   'edit_posts'=>'edit_branch_locations',
    //   'edit_private_posts'=>'edit_private_branch_locations',
    //   'edit_published_posts'=>'edit_published_branch_locations',
    //   'publish_posts'=>'publish_branch_locations',
    //   'read_private_posts'=>'read_private_branch_locations',
    //   'edit_post'=>'edit_branch_location',
    //   'delete_post'=>'delete_branch_location',
    //   'read_post'=>'read_branch_location',
    //   ),
	);
	register_post_type( 'locations', $args );

}
add_action( 'init', 'location_post_type', 0 );


function add_locations_meta_box() {
	add_meta_box(
		'locations_meta_box', // $id
		'Location Fields', // $title
		'show_locations_meta_box', // $callback
		'locations', // $screen
		'normal', // $context
		'high' // $priority
	);
}
add_action( 'add_meta_boxes', 'add_locations_meta_box' );


// Register Custom Post Type
function medical_emergency_post_type() {

	$labels = array(
		'name'                  => _x( 'Emergencies', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'emergency', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Emergencies', 'text_domain' ),
		'name_admin_bar'        => __( 'Medical Emergencies', 'text_domain' ),
		'archives'              => __( 'Emergency Archives', 'text_domain' ),
		'attributes'            => __( 'Emergency Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Emergencies', 'text_domain' ),
		'add_new_item'          => __( 'Add New Emergency', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Emergency', 'text_domain' ),
		'edit_item'             => __( 'Edit Emergency', 'text_domain' ),
		'update_item'           => __( 'Update Emergency', 'text_domain' ),
		'view_item'             => __( 'View Emergency', 'text_domain' ),
		'view_items'            => __( 'View Emergencies', 'text_domain' ),
		'search_items'          => __( 'Search Emergency', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'emergency', 'text_domain' ),
		'description'           => __( 'Allows admins to create and assign medical emergencies', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array('title', 'author', 'thumbnail', 'editor'),
		'taxonomies'            => array( 'post_tag' ),
    'menu_icon'             => 'dashicons-warning',
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 11,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'emergency', $args );

}
add_action( 'init', 'medical_emergency_post_type', 0 );

// Register Custom Post Type
function adopt_me_post_type() {

	$labels = array(
		'name'                  => _x( 'Adoptions', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Adoption', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Adopt Me Alerts', 'text_domain' ),
		'name_admin_bar'        => __( 'Adopt Me Alerts', 'text_domain' ),
		'archives'              => __( 'Adopt Me Archives', 'text_domain' ),
		'attributes'            => __( 'Adopt Me Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Adopt Me Alerts', 'text_domain' ),
		'add_new_item'          => __( 'Add New Adopt Me Alert', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Adopt Me Alert', 'text_domain' ),
		'edit_item'             => __( 'Edit Adopt Me Alert', 'text_domain' ),
		'update_item'           => __( 'Update Adopt Me Alert', 'text_domain' ),
		'view_item'             => __( 'View Adopt Me Alert', 'text_domain' ),
		'view_items'            => __( 'View Adopt Me Alerts', 'text_domain' ),
		'search_items'          => __( 'Search Adopt Me Alerts', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Adopt Me Alert list', 'text_domain' ),
		'items_list_navigation' => __( 'Adoptions list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter adoptions list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Adopt Me', 'text_domain' ),
		'description'           => __( 'Post type for Adopt Me posts', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', 'revisions', 'page-attributes', 'post-formats', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 12,
		'menu_icon'             => 'dashicons-heart',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,
		'exclude_from_search'   => true,
		'publicly_queryable'    => false,
		'capability_type'       => 'page',
	);
	register_post_type( 'adopt_me', $args );

}
add_action( 'init', 'adopt_me_post_type', 0 );


// Register Adoption Stories Post Type
function adoption_stories_post_type() {

	$labels = array(
		'name'                  => _x( 'Adoption Stories', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Adoption Story', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Adoption Stories', 'text_domain' ),
		'name_admin_bar'        => __( 'Adoption Story', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New Adoption Story', 'text_domain' ),
		'new_item'              => __( 'New Adoption Story', 'text_domain' ),
		'edit_item'             => __( 'Edit Adoption Story', 'text_domain' ),
		'update_item'           => __( 'Update Adoption Story', 'text_domain' ),
		'view_item'             => __( 'View Adoption Story', 'text_domain' ),
		'view_items'            => __( 'View Adoption Stories', 'text_domain' ),
		'search_items'          => __( 'Search Adoption Story', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Adoption Story', 'text_domain' ),
		'description'           => __( 'Custom post for adoption stories posted via Gravity Forms', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', 'page-attributes', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 13,
		'menu_icon'             => 'dashicons-smiley',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'adoption_stories', $args );

}
add_action( 'init', 'adoption_stories_post_type', 0 );



// Register Locations Custom Taxonomy
function locations_custom_taxonomies() {
    // Type of Locations Story taxonomy
    $labels = array(
        'name'              => 'Branch Locations',
        'singular_name'     => 'Branch Location',
        'search_items'      => 'Search Branch Locations',
        'all_items'         => 'All Branch Locations',
        'parent_item'       => 'Parent Type of Branch Location',
        'parent_item_colon' => 'Parent Type of Branch Location:',
        'edit_item'         => 'Edit Branch Location',
        'update_item'       => 'Update Branch Location',
        'add_new_item'      => 'Add a Branch Location',
        'new_item_name'     => 'New Type of Branch Location Name',
        'menu_name'         => 'Branch Locations',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'locations' ),
    );

    register_taxonomy( 'branch-location', array( 'locations', 'promos', 'news', 'adoption_stories', 'adopt_me', 'emergency', 'event'  ), $args );
 }

 add_action( 'init', 'locations_custom_taxonomies' );


// Register News Custom Taxonomy
function news_custom_taxonomies() {
    // Type of News Story taxonomy
    $labels = array(
        'name'              => 'News Categories',
        'singular_name'     => 'News Category',
        'search_items'      => 'Search News Categories',
        'all_items'         => 'All News Categories',
        'parent_item'       => 'Parent Type of News Story',
        'parent_item_colon' => 'Parent Type of News Story:',
        'edit_item'         => 'Edit News Category',
        'update_item'       => 'Update News Category',
        'add_new_item'      => 'Add a News Category',
        'new_item_name'     => 'New Type of News Category Name',
        'menu_name'         => 'News Categories',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'public'            => true,
        'publicly_queryable' => true,
        'show_ui'           => true,
        'show_in_nav_menus' => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'news-category' ),
    );

    register_taxonomy( 'news-type', array( 'news' ), $args );
 }

 add_action( 'init', 'news_custom_taxonomies', 0 );

if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
        'page_title' 	=> 'Theme General Settings',
        'menu_title'	=> 'Theme Settings',
        'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Header Settings',
        'menu_title'	=> 'Header',
        'parent_slug'	=> 'theme-general-settings',
    ));

    acf_add_options_sub_page(array(
        'page_title' 	=> 'Theme Footer Settings',
        'menu_title'	=> 'Footer',
        'parent_slug'	=> 'theme-general-settings',
    ));

}

 // function noindex_custom_posts() {
 //     if ( is_singular( 'promos' ) ) {
 //         echo '<meta name="robots" content="noindex, follow">';
 //     }
 // }
 //
 // add_action('wp_head', 'noindex_custom_posts')






// Jetpack Related Posts - uncomment functions below when Jetpack is approved


// function jetpackme_allow_pages_for_relatedposts( $enabled ) {
//     if ( is_page() ) {
//         $enabled = true;
//     }
//     return $enabled;
// }        
// add_filter( 'jetpack_relatedposts_filter_enabled_for_request', 'jetpackme_allow_pages_for_relatedposts' );


// function jetpackme_exclude_related_post( $exclude_post_ids, $post_id ) {
//     // $post_id is the post we are currently getting related posts for
//     $exclude_post_ids[] = 29;
//     $exclude_post_ids[] = 9588;
//     $exclude_post_ids[] = 5447;
//     $exclude_post_ids[] = 7476;
//     $exclude_post_ids[] = 4881;
//     $exclude_post_ids[] = 4212;
//     return $exclude_post_ids;
// }
// add_filter( 'jetpack_relatedposts_filter_exclude_post_ids', 'jetpackme_exclude_related_post', 20, 2 );
// 


// alter archive query where if post type is news, show 12 posts per page
function yd_news_query( $query ){
    if( ! is_admin()
        && $query->is_post_type_archive( 'news' )
        && $query->is_main_query() ) {
            $query->set( 'posts_per_page', 12 );
    }
}
add_action( 'pre_get_posts', 'yd_news_query' );



function yd_my_load_more_scripts() {
 
	global $wp_query; 
 
	// In most cases it is already included on the page and this line can be removed
	wp_enqueue_script('jquery');
 
	// register our main script but do not enqueue it yet
	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/myloadmore.js', array('jquery') );
 
	// now the most interesting part
	// we have to pass parameters to myloadmore.js script but we can get the parameters values only in PHP
	// you can define variables directly in your HTML but I decided that the most proper way is wp_localize_script()
	wp_localize_script( 'my_loadmore', 'yd_loadmore_params', array(
		'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page' => $wp_query->max_num_pages
	) );
 
 	wp_enqueue_script( 'my_loadmore' );
}
add_action( 'wp_enqueue_scripts', 'yd_my_load_more_scripts' );




function yd_loadmore_ajax_handler() {
	
		global $promo_count;

	if ($promo_count > 3) { $promo_count = 1;}
 
	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';
 
	// it is always better to use WP_Query but not here
	query_posts( $args );
 
	if( have_posts() ) :
 
		// run the loop
		while( have_posts() ): the_post();
 
			// look into your theme code how the posts are inserted, but you can use your own HTML of course
			// do you remember? - my example is adapted for Twenty Seventeen theme
			get_template_part( 'template-parts/post/archive', get_post_format() );
			// for the test purposes comment the line above and uncomment the below one
			// the_title();
 
 
		endwhile;
 
	endif;
	die; // here we exit the script and even no wp_reset_query() required!
}
add_action('wp_ajax_loadmore', 'yd_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'yd_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}

/**
 * ACF Options Page 
 *
 */
function yd_acf_news_page() {
    if ( function_exists( 'acf_add_options_sub_page' ) ){
 		 acf_add_options_sub_page( array(
			'title'      => 'News Stories Settings',
			'parent'     => 'edit.php?post_type=news',
			'capability' => 'manage_options'
		) );
 	}
}
add_action( 'init', 'yd_acf_news_page' );


function get_modified_term_list( $id = 0, $taxonomy, $before = '', $sep = '', $after = '', $exclude = array() ) {
    $terms = get_the_terms( $id, $taxonomy );

    if ( is_wp_error( $terms ) )
        return $terms;

    if ( empty( $terms ) )
        return false;

    foreach ( $terms as $term ) {

        if(!in_array($term->slug,$exclude)) {
            $link = get_term_link( $term, $taxonomy );
            if ( is_wp_error( $link ) )
                return $link;
            $term_links[] = '<a href="' . $link . '" rel="tag">' . $term->name . '</a>';
        }
    }

    if( !isset( $term_links ) )
        return false;

    return $before . join( $sep, $term_links ) . $after;
}